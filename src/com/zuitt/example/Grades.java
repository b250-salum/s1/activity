package com.zuitt.example;

import java.util.Scanner;
public class Grades {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);


        System.out.println("First Name:");
        String firstname = myObj.nextLine();
        System.out.println("Last Name:");
        String lastname = myObj.nextLine();
        System.out.println("First Subject Grade:");
        double subj1 = myObj.nextDouble();
        System.out.println("Second Subject Grade:");
        double subj2 = myObj.nextDouble();
        System.out.println("Third Subject Grade:");
        double subj3 = myObj.nextDouble();
        System.out.println("Forth Subject Grade:");
        double subj4 = myObj.nextDouble();
        System.out.println("Good day, " + firstname + " " + lastname);
        double gradeAverage = ((subj1 + subj2 + subj3 + subj4)/4);
        System.out.println("Your grade average is: " + gradeAverage);
    }
}
